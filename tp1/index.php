<?php
session_start();
include('game.php');

$nmbrLig = 10;
$nmbrCol = 10;
if(isset($_GET['reset']) && !empty($_GET['reset'])){
  $_SESSION['generation'] = [];
}
$generationActuelle = [];
$generationFuture = [];

if(!isset($_SESSION['generation']) || empty($_SESSION['generation'])){
  $generationActuelle = initialiserTab($generationActuelle, $nmbrLig, $nmbrCol);
  $_SESSION['generation'] = $generationActuelle;
} else {
  $generationActuelle = $_SESSION['generation'];
  $generationFuture = genFuture($generationActuelle, $generationFuture, $nmbrLig, $nmbrCol);
  $_SESSION['generation'] = $generationFuture;
  $generationActuelle = $generationFuture;
}

include('grid_template.html');
