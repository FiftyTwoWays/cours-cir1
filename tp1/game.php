<?php
function initialiserTab($tab, $nmbrLig, $nmbrCol){
  for($i = 0; $i < $nmbrLig; $i++) {
    for($j = 0; $j < $nmbrCol; $j++) {
      if(rand(0,1) == 0){
        $tab[$i][$j] = 'o';
      } else {
        $tab[$i][$j] = '_';
      }
    }
  }
  return $tab;
}

function afficherGen($tab, $nmbrLig, $nmbrCol){
  for($i = 0; $i < $nmbrLig; $i++) {
    echo '<tr>';
    for($j = 0; $j < $nmbrCol; $j++) {
      echo '<td>' . $tab[$i][$j] . '</td>';
    }
    echo '</tr>';
  }
}

function nmbrCellulesVoisinesVivantes($tab, $ligne, $colonne, $nmbrLig, $nmbrCol){
  $resultat = 0;

  if($ligne == 0 && $colonne == 0){
    for($i = $ligne; $i <= $ligne + 1; $i++){
      for($j = $colonne; $j <= $colonne + 1; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } elseif ($ligne == 0 && $colonne == ($nmbrCol - 1)) {
    for($i = $ligne; $i <= $ligne + 1; $i++){
      for($j = $colonne - 1; $j <= $colonne; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } elseif ($ligne == ($nmbrLig - 1) && $colonne == 0) {
    for($i = $ligne - 1; $i <= $ligne; $i++){
      for($j = $colonne; $j <= $colonne + 1; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } elseif ($ligne == ($nmbrLig - 1) && $colonne == ($nmbrCol - 1)) {
    for($i = $ligne - 1; $i <= $ligne; $i++){
      for($j = $colonne - 1; $j <= $colonne; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } elseif ($ligne == 0) {
    for($i = $ligne; $i <= $ligne + 1; $i++){
      for($j = $colonne - 1; $j <= $colonne + 1; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } elseif ($ligne == ($nmbrLig - 1)) {
    for($i = $ligne - 1; $i <= $ligne; $i++){
      for($j = $colonne - 1; $j <= $colonne + 1; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } elseif ($colonne == 0) {
    for($i = $ligne - 1; $i <= $ligne + 1; $i++){
      for($j = $colonne; $j <= $colonne + 1; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } elseif ($colonne == ($nmbrCol - 1)) {
    for($i = $ligne - 1; $i <= $ligne + 1; $i++){
      for($j = $colonne - 1; $j <= $colonne; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  } else {
    for($i = $ligne - 1; $i <= $ligne + 1; $i++){
      for($j = $colonne - 1; $j <= $colonne + 1; $j++){
        if($tab[$i][$j] == 'o'){
          $resultat++;
        }
      }
    }
    if($tab[$ligne][$colonne] == 'o'){
        $resultat--;
    }
    return $resultat;
  }
}

function genFuture($tabActuelle, $tabFuture, $nmbrLig, $nmbrCol){
  for($i = 0; $i < $nmbrLig; $i++) {
    for($j = 0; $j < $nmbrCol; $j++) {
      if(nmbrCellulesVoisinesVivantes($tabActuelle, $i, $j, $nmbrLig, $nmbrCol) < 2 || nmbrCellulesVoisinesVivantes($tabActuelle, $i, $j, $nmbrLig, $nmbrCol) >= 4){
        $tabFuture[$i][$j] = '_';
      } elseif (nmbrCellulesVoisinesVivantes($tabActuelle, $i, $j, $nmbrLig, $nmbrCol) == 3 && $tabActuelle[$i][$j] == '_') {
        $tabFuture[$i][$j] = 'o';
      } else {
        $tabFuture[$i][$j] = $tabActuelle[$i][$j];
      }
    }
  }
  return $tabFuture;
}
